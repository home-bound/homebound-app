export type ProfileDiary = {
  PeopleMet: 0 | 1 | 2 | 3 | 4 | 5;
  Hygiene: 0 | 1 | 2 | 3;
};

type RiskAreaCountry =
  | 'none'
  | 'germany'
  | 'austria'
  | 'france'
  | 'italy'
  | 'spain'
  | 'iran'
  | 'usa'
  | 'china'
  | 'south_korea'
  | 'multiple';

export type UserProfile = {
  Name?: string;
  Height?: number;
  Weight?: number;
  Age?: number;
  BiologicalSex?: 'male' | 'female' | 'diverse';
  Restrictions?: 0 | 1 | 2 | 3 | 4;
  VisitedRiskArea?: RiskAreaCountry;
  VisitedRiskAreaDuration?: 0 | 1 | 2 | 3 | 7;
  HomeOfficeApproval?: 0 | 1 | 2 | 3;
  BehaviourApproval?: 0 | 1 | 2 | 3 | 4;
  RiskPersonsAround?: 0 | 1 | 2 | 3;
  Diary?: Record<string, ProfileDiary>;
};
