import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  Picker,
  TextInput,
} from 'react-native';
import AppleHealthKit from 'rn-apple-healthkit';
import {withTranslation, WithTranslation} from 'react-i18next';

import {styles} from '../../utils/theme';
import {UserProfileContext} from '../../utils/context/userProfileContext';

type ProfileState = {};
type ProfileProps = {} & WithTranslation;

class Profile extends Component<ProfileProps, ProfileState> {
  static contextType = UserProfileContext;
  context!: React.ContextType<typeof UserProfileContext>;

  constructor(props: ProfileProps) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    let options = {
      permissions: {
        read: ['Height', 'Weight', 'DateOfBirth', 'BiologicalSex'],
        write: [],
      },
    };

    AppleHealthKit.initHealthKit(options, (err, results) => {
      if (err) {
        console.log('error initializing Healthkit: ', err);
        return;
      }

      AppleHealthKit.getDateOfBirth(null, (err, results) => {
        const value = results?.age;
        !this.context.profile.Age &&
          value &&
          this.context.updateProfile({Age: value});
      });

      // Ignore typescript error :(
      AppleHealthKit.getLatestHeight(
        {unit: 'meter'},
        (err: any, results: {value: any}) => {
          const value = results?.value;
          !this.context.profile.Height &&
            value &&
            this.context.updateProfile({Height: Math.round(value * 100) / 100});
        },
      );

      // Ignore typescript error :(
      AppleHealthKit.getLatestWeight(
        {unit: 'gram'},
        (err: any, results: {value: any}) => {
          const value = results?.value;
          !this.context.profile.Weight &&
            value &&
            this.context.updateProfile({Weight: Math.round(value / 100) / 10});
        },
      );

      // Ignore typescript error :(
      AppleHealthKit.getBiologicalSex(
        null,
        (err: any, results: {value: any}) => {
          const value = results?.value;
          !this.context.profile.BiologicalSex &&
            value &&
            this.context.updateProfile({BiologicalSex: value});
        },
      );
    });
  }

  renderPickerItems(data: Record<string | number, string>) {
    return Object.entries(
      data,
    ).map(([key, value]: [string, string], index: number) => (
      <Picker.Item key={index} label={value} value={key} />
    ));
  }

  renderSelector(key: string) {
    const {t} = this.props;

    return (
      <View style={styles.sectionContainer}>
        <Text>{t(`profile.options.${key}.description`)}</Text>
        <Picker
          itemStyle={{height: 120}}
          selectedValue={this.context.profile[key]}
          onValueChange={itemValue =>
            this.context.updateProfile({[key]: itemValue})
          }>
          {this.renderPickerItems(
            t(`profile.options.${key}.values`, {returnObjects: true}),
          )}
        </Picker>
      </View>
    );
  }

  render() {
    const {t} = this.props;
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <Text style={styles.appTitle}>
                  {t('profile.title', {
                    name:
                      this.context.profile.Name || t('profile.default name'),
                  })}
                </Text>
                <Text>{t('profile.options.Name.description')}</Text>
                <TextInput
                  style={styles.input}
                  onChangeText={text =>
                    this.context.updateProfile({Name: text})
                  }
                  value={this.context.profile.Name?.toString()}
                />
              </View>

              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>
                  {t('profile.Demographics')}
                </Text>
                <Text>{t('profile.options.Age.description')}</Text>
                <TextInput
                  style={styles.input}
                  keyboardType="numeric"
                  onChangeText={text =>
                    this.context.updateProfile({Age: parseInt(text)})
                  }
                  value={this.context.profile.Age?.toString()}
                />
              </View>

              {this.renderSelector('BiologicalSex')}

              <View style={styles.sectionContainer}>
                <Text>{t('profile.options.Height.description')}</Text>
                <TextInput
                  style={styles.input}
                  keyboardType="numeric"
                  onChangeText={text =>
                    this.context.updateProfile({Height: parseInt(text)})
                  }
                  value={this.context.profile.Height?.toString()}
                />
              </View>

              <View style={styles.sectionContainer}>
                <Text>{t('profile.options.Weight.description')}</Text>
                <TextInput
                  style={styles.input}
                  keyboardType="numeric"
                  onChangeText={text =>
                    this.context.updateProfile({Weight: parseInt(text)})
                  }
                  value={this.context.Weight?.toString()}
                />
              </View>

              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>
                  {t('profile.Social distancing')}
                </Text>
              </View>
              {this.renderSelector('Restrictions')}

              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>{t('Risk Areas')}</Text>
              </View>
              {this.renderSelector('VisitedRiskArea')}
              {this.renderSelector('VisitedRiskAreaDuration')}
              {this.renderSelector('HomeOfficeApproval')}
              {this.renderSelector('BehaviourApproval')}

              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>{t('Risk Group')}</Text>
              </View>
              {this.renderSelector('RiskPersonsAround')}
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

export default withTranslation()(Profile);
