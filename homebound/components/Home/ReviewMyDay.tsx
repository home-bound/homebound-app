import React, {useContext} from 'react';
import {View, Text, Picker} from 'react-native';
import {useTranslation} from 'react-i18next';
import Slider from '@react-native-community/slider';

import {styles} from '../../utils/theme';
import {UserProfileContext} from '../../utils/context/userProfileContext';
import {ProfileDiary} from 'root-types';

const ReviewMyDay = () => {
  const {t} = useTranslation();
  const {profile, updateProfile} = useContext(UserProfileContext);

  const getTodayString = () => new Date().toLocaleDateString();
  const todayDiary: ProfileDiary =
    (profile.Diary && profile.Diary[getTodayString()]) || {};

  const updateDiary = (change: Partial<ProfileDiary>) => {
    const update = {Diary: {}};
    update['Diary'][getTodayString()] = change;
    updateProfile(update);
  };

  const renderPickerItems = (data: Record<string | number, string>) => {
    return Object.entries(
      data,
    ).map(([key, value]: [string, string], index: number) => (
      <Picker.Item key={index} label={value} value={key} />
    ));
  };

  return (
    <>
      {/* {renderSelector('PeopleMet')} */}
      <View style={styles.sectionContainer}>
        <Text>{t('review.options.PeopleMet.description')}</Text>
        <Slider
          minimumValue={0}
          maximumValue={5}
          step={1}
          value={todayDiary.PeopleMet}
          onValueChange={itemValue =>
            updateDiary({PeopleMet: Math.round(itemValue)})
          }
        />
        <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
          {t('review.options.PeopleMet.label', {n: todayDiary.PeopleMet})}
        </Text>
      </View>

      <View style={styles.sectionContainer}>
        <Text>{t('review.options.Hygiene.description')}</Text>
        <Picker
          selectedValue={todayDiary.Hygiene}
          itemStyle={{height: 88}}
          onValueChange={itemValue => updateDiary({Hygiene: itemValue})}>
          {renderPickerItems(
            t('review.options.Hygiene.values', {returnObjects: true}),
          )}
        </Picker>
      </View>
    </>
  );
};
export default ReviewMyDay;
