import React from 'react';
import { Path } from 'react-native-svg';
import {
  StackedAreaChart,
  AreaChart,
  Grid,
  XAxis,
  YAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import { View } from 'react-native';
import { format } from 'date-fns';

const CoronaAreaChart = ({ color }: { color: string }) => {

  const dates = ['2020-01-27', '2020-01-28', '2020-01-29', '2020-01-30', '2020-01-31', '2020-02-01', '2020-02-02', '2020-02-03', '2020-02-04', '2020-02-05', '2020-02-06', '2020-02-07', '2020-02-08', '2020-02-09', '2020-02-10', '2020-02-11', '2020-02-12', '2020-02-13', '2020-02-14', '2020-02-15', '2020-02-16', '2020-02-17', '2020-02-18', '2020-02-19', '2020-02-20', '2020-02-21', '2020-02-22', '2020-02-23', '2020-02-24', '2020-02-25', '2020-02-26', '2020-02-27', '2020-02-28', '2020-02-29', '2020-03-01', '2020-03-02', '2020-03-03', '2020-03-04', '2020-03-05', '2020-03-06', '2020-03-07', '2020-03-08', '2020-03-09', '2020-03-10', '2020-03-11', '2020-03-12', '2020-03-13', '2020-03-14', '2020-03-15', '2020-03-16', '2020-03-17', '2020-03-18', '2020-03-19', '2020-03-20', '2020-03-21', '2020-03-22', '2020-03-23', '2020-03-24', '2020-03-25', '2020-03-26', '2020-03-27', '2020-03-28', '2020-03-29', '2020-03-30', '2020-03-31', '2020-04-01', '2020-04-02', '2020-04-03', '2020-04-04', '2020-04-05', '2020-04-06', '2020-04-07', '2020-04-08', '2020-04-09', '2020-04-10', '2020-04-11', '2020-04-12', '2020-04-13', '2020-04-14', '2020-04-15', '2020-04-16', '2020-04-17', '2020-04-18', '2020-04-19', '2020-04-20', '2020-04-21', '2020-04-22', '2020-04-23', '2020-04-24', '2020-04-25', '2020-04-26', '2020-04-27', '2020-04-28', '2020-04-29', '2020-04-30', '2020-05-01', '2020-05-02', '2020-05-03', '2020-05-04', '2020-05-05', '2020-05-06', '2020-05-07', '2020-05-08', '2020-05-09', '2020-05-10', '2020-05-11', '2020-05-12', '2020-05-13', '2020-05-14', '2020-05-15', '2020-05-16', '2020-05-17'];
  const data = [{ 'date': '2020-01-27', 'active': 1.0, 'active_predicted': 0 }, { 'date': '2020-01-28', 'active': 4.0, 'active_predicted': 0 }, { 'date': '2020-01-29', 'active': 4.0, 'active_predicted': 0 }, { 'date': '2020-01-30', 'active': 4.0, 'active_predicted': 0 }, { 'date': '2020-01-31', 'active': 5.0, 'active_predicted': 0 }, { 'date': '2020-02-01', 'active': 8.0, 'active_predicted': 0 }, { 'date': '2020-02-02', 'active': 10.0, 'active_predicted': 0 }, { 'date': '2020-02-03', 'active': 12.0, 'active_predicted': 0 }, { 'date': '2020-02-04', 'active': 12.0, 'active_predicted': 0 }, { 'date': '2020-02-05', 'active': 12.0, 'active_predicted': 0 }, { 'date': '2020-02-06', 'active': 12.0, 'active_predicted': 0 }, { 'date': '2020-02-07', 'active': 13.0, 'active_predicted': 0 }, { 'date': '2020-02-08', 'active': 13.0, 'active_predicted': 0 }, { 'date': '2020-02-09', 'active': 14.0, 'active_predicted': 0 }, { 'date': '2020-02-10', 'active': 14.0, 'active_predicted': 0 }, { 'date': '2020-02-11', 'active': 16.0, 'active_predicted': 0 }, { 'date': '2020-02-12', 'active': 16.0, 'active_predicted': 0 }, { 'date': '2020-02-13', 'active': 15.0, 'active_predicted': 0 }, { 'date': '2020-02-14', 'active': 15.0, 'active_predicted': 0 }, { 'date': '2020-02-15', 'active': 15.0, 'active_predicted': 0 }, { 'date': '2020-02-16', 'active': 15.0, 'active_predicted': 0 }, { 'date': '2020-02-17', 'active': 15.0, 'active_predicted': 0 }, { 'date': '2020-02-18', 'active': 4.0, 'active_predicted': 0 }, { 'date': '2020-02-19', 'active': 4.0, 'active_predicted': 0 }, { 'date': '2020-02-20', 'active': 4.0, 'active_predicted': 0 }, { 'date': '2020-02-21', 'active': 2.0, 'active_predicted': 0 }, { 'date': '2020-02-22', 'active': 2.0, 'active_predicted': 0 }, { 'date': '2020-02-23', 'active': 2.0, 'active_predicted': 0 }, { 'date': '2020-02-24', 'active': 2.0, 'active_predicted': 0 }, { 'date': '2020-02-25', 'active': 3.0, 'active_predicted': 0 }, { 'date': '2020-02-26', 'active': 12.0, 'active_predicted': 0 }, { 'date': '2020-02-27', 'active': 30.0, 'active_predicted': 0 }, { 'date': '2020-02-28', 'active': 32.0, 'active_predicted': 0 }, { 'date': '2020-02-29', 'active': 63.0, 'active_predicted': 0 }, { 'date': '2020-03-01', 'active': 114.0, 'active_predicted': 0 }, { 'date': '2020-03-02', 'active': 143.0, 'active_predicted': 0 }, { 'date': '2020-03-03', 'active': 180.0, 'active_predicted': 0 }, { 'date': '2020-03-04', 'active': 246.0, 'active_predicted': 0 }, { 'date': '2020-03-05', 'active': 466.0, 'active_predicted': 0 }, { 'date': '2020-03-06', 'active': 653.0, 'active_predicted': 0 }, { 'date': '2020-03-07', 'active': 781.0, 'active_predicted': 0 }, { 'date': '2020-03-08', 'active': 1022.0, 'active_predicted': 0 }, { 'date': '2020-03-09', 'active': 1156.0, 'active_predicted': 0 }, { 'date': '2020-03-10', 'active': 1437.0, 'active_predicted': 0 }, { 'date': '2020-03-11', 'active': 1880.0, 'active_predicted': 0 }, { 'date': '2020-03-12', 'active': 2050.0, 'active_predicted': 0 }, { 'date': '2020-03-13', 'active': 3622.0, 'active_predicted': 0 }, { 'date': '2020-03-14', 'active': 4530.0, 'active_predicted': 0 }, { 'date': '2020-03-15', 'active': 5738.0, 'active_predicted': 0 }, { 'date': '2020-03-16', 'active': 7188.0, 'active_predicted': 0 }, { 'date': '2020-03-17', 'active': 9166.0, 'active_predicted': 0 }, { 'date': '2020-03-18', 'active': 12194.0, 'active_predicted': 0 }, { 'date': '2020-03-19', 'active': 15163.0, 'active_predicted': 0 }, { 'date': '2020-03-20', 'active': 19601.0, 'active_predicted': 0 }, { 'date': '2020-03-21', 'active': 21896.0, 'active_predicted': 0 }, { 'date': '2020-03-22', 'active': 24513.0, 'active_predicted': 0 }, { 'date': '2020-03-23', 'active': 0, 'active_predicted': 28533.0 }, { 'date': '2020-03-24', 'active': 0, 'active_predicted': 31777.0 }, { 'date': '2020-03-25', 'active': 0, 'active_predicted': 34696.0 }, { 'date': '2020-03-26', 'active': 0, 'active_predicted': 37140.0 }, { 'date': '2020-03-27', 'active': 0, 'active_predicted': 38977.0 }, { 'date': '2020-03-28', 'active': 0, 'active_predicted': 40103.0 }, { 'date': '2020-03-29', 'active': 0, 'active_predicted': 40453.0 }, { 'date': '2020-03-30', 'active': 0, 'active_predicted': 40005.0 }, { 'date': '2020-03-31', 'active': 0, 'active_predicted': 38786.0 }, { 'date': '2020-04-01', 'active': 0, 'active_predicted': 36868.0 }, { 'date': '2020-04-02', 'active': 0, 'active_predicted': 34357.0 }, { 'date': '2020-04-03', 'active': 0, 'active_predicted': 31389.0 }, { 'date': '2020-04-04', 'active': 0, 'active_predicted': 28115.0 }, { 'date': '2020-04-05', 'active': 0, 'active_predicted': 24689.0 }, { 'date': '2020-04-06', 'active': 0, 'active_predicted': 21256.0 }, { 'date': '2020-04-07', 'active': 0, 'active_predicted': 17941.0 }, { 'date': '2020-04-08', 'active': 0, 'active_predicted': 14846.0 }, { 'date': '2020-04-09', 'active': 0, 'active_predicted': 12044.0 }, { 'date': '2020-04-10', 'active': 0, 'active_predicted': 9579.0 }, { 'date': '2020-04-11', 'active': 0, 'active_predicted': 7470.0 }, { 'date': '2020-04-12', 'active': 0, 'active_predicted': 5710.0 }, { 'date': '2020-04-13', 'active': 0, 'active_predicted': 4280.0 }, { 'date': '2020-04-14', 'active': 0, 'active_predicted': 3145.0 }, { 'date': '2020-04-15', 'active': 0, 'active_predicted': 2265.0 }, { 'date': '2020-04-16', 'active': 0, 'active_predicted': 1600.0 }, { 'date': '2020-04-17', 'active': 0, 'active_predicted': 1108.0 }, { 'date': '2020-04-18', 'active': 0, 'active_predicted': 752.0 }, { 'date': '2020-04-19', 'active': 0, 'active_predicted': 501.0 }, { 'date': '2020-04-20', 'active': 0, 'active_predicted': 327.0 }, { 'date': '2020-04-21', 'active': 0, 'active_predicted': 209.0 }, { 'date': '2020-04-22', 'active': 0, 'active_predicted': 131.0 }, { 'date': '2020-04-23', 'active': 0, 'active_predicted': 81.0 }, { 'date': '2020-04-24', 'active': 0, 'active_predicted': 49.0 }, { 'date': '2020-04-25', 'active': 0, 'active_predicted': 29.0 }, { 'date': '2020-04-26', 'active': 0, 'active_predicted': 17.0 }, { 'date': '2020-04-27', 'active': 0, 'active_predicted': 9.0 }, { 'date': '2020-04-28', 'active': 0, 'active_predicted': 5.0 }, { 'date': '2020-04-29', 'active': 0, 'active_predicted': 3.0 }, { 'date': '2020-04-30', 'active': 0, 'active_predicted': 2.0 }, { 'date': '2020-05-01', 'active': 0, 'active_predicted': 1.0 }, { 'date': '2020-05-02', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-03', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-04', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-05', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-06', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-07', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-08', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-09', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-10', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-11', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-12', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-13', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-14', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-15', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-16', 'active': 0, 'active_predicted': 0.0 }, { 'date': '2020-05-17', 'active': 0, 'active_predicted': 0.0 }];
  const keys = ['active', 'active_predicted'];
  const colors = [color, `${color}80`];

  const Line = ({ line }: any) => (
    <Path key={'line'} d={line} stroke={'rgb(134, 65, 244)'} fill={'none'} />
  );

  const mapDate = (dates, index) => {
    var date = Date.parse(dates[index]);
    if (format(date, 'd') === '1') {
      return format(date, 'MMM');
    }
    return '';
  };

  return (
    <View style={{ height: 280, padding: 20 }}>
      <StackedAreaChart
        style={{ flex: 1 }}
        data={data}
        keys={keys}
        colors={colors}
        contentInset={{ top: 30, bottom: 30 }}
        numberOfTicks={5}>
        <Grid direction={'HORIZONTAL'} />
        <Line />
      </StackedAreaChart>
      <XAxis
        style={{ marginHorizontal: -10, marginTop: -20 }}
        data={data}
        formatLabel={(value, index) => mapDate(dates, index)}
        contentInset={{ left: 10, right: 10 }}
        svg={{ fontSize: 10, fill: 'black' }}
      />
    </View>
  );
};

export default CoronaAreaChart;
