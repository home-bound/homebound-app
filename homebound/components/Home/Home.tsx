import React, {useContext} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useTranslation} from 'react-i18next';

import {styles} from '../../utils/theme';
import {RootStackParamList} from 'App';
import CoronaAreaChart from './CoronaAreaChart';
import Leaderboard from './Leaderboard';
import EducationalTile from './EducationalTile';
import {UserProfileContext} from '../../utils/context/userProfileContext';
import ReviewMyDay from './ReviewMyDay';
import {ProfileDiary} from 'root-types';

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList>;
type Props = {
  navigation: HomeScreenNavigationProp;
};

const Home = ({navigation}: Props) => {
  const {t} = useTranslation();
  const {profile} = useContext(UserProfileContext);

  const getTodayString = () => new Date().toLocaleDateString();
  const todayDiary: ProfileDiary =
    (profile.Diary && profile.Diary[getTodayString()]) || {};

  const dailyScore = Math.max(
    0,
    100 -
      Math.round(
        ((todayDiary.PeopleMet || 0) * 15) ** 1.15 +
          ((todayDiary.PeopleMet || 0) + 1) *
            ((todayDiary.Hygiene || 1) * 3) ** 1.5,
      ),
  );

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.appTitle}>{t('home.title')}</Text>
              <Text>{t('home.subtitle')}</Text>
            </View>

            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>
                {t('home.Your daily impact')}
              </Text>

              <View style={styles.cardWide}>
                <Text style={styles.cardInnerTitle}>
                  {t('review.title', {
                    name: profile.Name || t('review.default name'),
                  })}
                </Text>
                <ReviewMyDay />
              </View>

              <View style={styles.cardWide}>
                <Text style={styles.cardInnerTitle}>
                  {t('home.daily-saved', {n: 9})}
                </Text>
                <Text style={{paddingTop: 10}}>
                  {t('home.daily-score', {n: dailyScore})}
                </Text>
              </View>

              <View style={styles.cardWide}>
                <Text style={styles.cardInnerTitle}>
                  {t('home.Your impact on the curve')}
                </Text>
                <CoronaAreaChart
                  color={
                    dailyScore < 33
                      ? '#de7119'
                      : dailyScore < 66
                      ? '#f6d186'
                      : '#2b580c'
                  }
                />
              </View>

              {/* <View style={styles.cardWide}>
                <Text style={styles.cardInnerTitle}>Risk Profile</Text>
                <Text style={{paddingTop: 10}}>
                  Medium risk
                </Text>
              </View> */}
            </View>

            <Leaderboard />

            <EducationalTile />

            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>{t('home.You')}</Text>
              <View style={styles.cardWide}>
                <Button
                  title={t('home.Update Profile')}
                  onPress={() => navigation.navigate('Profile')}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Home;
