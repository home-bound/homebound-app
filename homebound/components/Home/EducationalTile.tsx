import React from 'react';
import {View, Text, Image} from 'react-native';
import {styles} from '../../utils/theme';
import { useTranslation } from 'react-i18next';

const imgHouseSrc = require('../../utils/assets/images/house.jpg');
const imgWashSrc = require('../../utils/assets/images/wash-hands.jpg');

const Card = ({title, image, description}) => (
  <View style={styles.educationalCard}>
    <Image style={styles.educationalCardImage} source={image} />
    <View style={styles.educationalCardContent}>
      <Text style={styles.sectionTitle}>{title}</Text>
      <Text style={styles.descriptionText}>{description}</Text>
    </View>
  </View>
);

const EducationalTile = () => {
  const {t} = useTranslation();

  return (
    <View style={styles.sectionContainer}>
      <Card
        title={t('home.education.Social-Distancing-Title')}
        image={imgHouseSrc}
        description={t('home.education.Social-Distancing-Desc')}
      />

      <Card
        title={t('home.education.Hygiene-Title')}
        image={imgWashSrc}
        description={t('home.education.Hygiene-Desc')}
      />
    </View>
  );
};

export default EducationalTile;
