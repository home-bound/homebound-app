import React from 'react';
import {View, Text, Button} from 'react-native';
import {styles} from '../../utils/theme';

class Leaderboard extends React.PureComponent {
  render() {
    const data = [
      {
        id: '445de681-a3da-4b48-b8b4-f3b081fc7278',
        name: 'Martin Z.',
        score: 2.4,
        positive: true,
      },
      {
        id: '97146d73-208c-4f54-8bc7-af29f0adf37a',
        name: 'Max',
        score: 3.2,
        positive: false,
      },
      {
        id: '327b5705-946d-4b5f-856e-6fc2130f5639',
        name: 'Anna',
        score: 9.0,
        positive: true,
      },
      {
        id: '9fc60dbb-2041-4ad7-ba5b-f6dd200c9a12',
        name: 'Martin',
        score: 1.9,
        positive: true,
      },
      {
        id: '8b55571c-f14c-47ce-bd32-db283c2fb6b2',
        name: 'Regina',
        score: 4.1,
        positive: true,
      },
    ];

    function ScoreDisplay({score, positive}): any {
      return positive ? (
        <View style={styles.itemPositive}>
          <Button title="🎉" />
          <Text style={styles.itemScore}>{score}</Text>
          <Text>lifes saved</Text>
        </View>
      ) : (
        <View style={styles.itemNegative}>
          <Button title="🤦🏻‍♀️" />
          <Text style={styles.itemScore}>{score}</Text>
          <Text>lifes endangered</Text>
        </View>
      );
    }

    function Item({name, score, positive}): any {
      return (
        <View style={styles.item}>
          <Text style={styles.itemName}>{name}</Text>
          <ScoreDisplay score={score} positive={positive} />
        </View>
      );
    }

    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionTitle}>Spread the vibe</Text>

        {data.map((item, index) => (
          <Item
            key={index}
            score={item.score}
            positive={item.positive}
            name={item.name}
          />
        ))}
      </View>
    );
  }
}

export default Leaderboard;
