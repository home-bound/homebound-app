import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: 'white',
  },
  card: {
    // boxShadow: '0 1px 1px rgba(0, 0, 0, .04), 0 4px 5px rgba(0, 0, 0, .02), 0 7px 9px rgba(0, 0, 0, .04)',
    borderRadius: 25,
    margin: 15,
  },
  itemName: {
    fontSize: 18,
    fontWeight: '700',
  },
  educationalCardImage: {
    width: 'auto',
    height: 200,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    resizeMode: 'stretch',
  },
  descriptionText: {
    paddingTop: 5,
  },
  educationalCard: {
    backgroundColor: '#f0f0f0',
    marginBottom: 10,
    borderRadius: 15,
    color: 'black',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2,
    shadowRadius: 10,
    marginTop: 30,
  },
  educationalCardContent: {
    padding: 10,
    marginTop: 5,
    marginBottom: 10,
  },
  itemPositive: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    color: 'green',
  },
  itemNegative: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  item: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f0f0f0',
    marginBottom: 5,
    borderRadius: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    color: 'black',
  },
  itemScore: {
    fontSize: 32,
  },
  input: {
    height: 40,
    borderColor: '#ccc',
    borderRadius: 15,
    borderWidth: 1,
    marginTop: 10,
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  cardWide: {
    backgroundColor: '#f0f0f0',
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 15,
    padding: 10,
    color: 'black',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: 'black',
    marginBottom: 5,
  },
  cardInnerTitle: {
    fontSize: 18,
    fontWeight: '600',
    color: 'black',
  },
  appTitle: {
    fontSize: 30,
    fontWeight: '700',
    color: 'black',
    marginBottom: 10,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: '#333',
  },
  highlight: {
    fontWeight: '700',
  },
});
