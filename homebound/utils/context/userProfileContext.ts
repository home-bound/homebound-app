import React from 'react';

import {UserProfile} from 'root-types';

type UserProfileContextType = {
  profile: UserProfile;
  updateProfile: (change: Partial<UserProfile>) => void;
};

export const UserProfileContext = React.createContext<UserProfileContextType>({
  profile: {},
  updateProfile: () => {},
});
