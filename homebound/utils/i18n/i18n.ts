import * as RNLocalize from 'react-native-localize';
import i18n, {LanguageDetectorModule} from 'i18next';
import {initReactI18next} from 'react-i18next';

import translationEn from './en';
import translationDe from './de';

const resources = {
  en: {translation: translationEn},
  de: {translation: translationDe},
};

const languageDetector: LanguageDetectorModule = {
  type: 'languageDetector',
  detect: () =>
    RNLocalize.findBestAvailableLanguage(Object.keys(resources))?.languageTag,
  init: () => {},
  cacheUserLanguage: () => {},
};

i18n
  .use(languageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: 'en',
    debug: true,
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
