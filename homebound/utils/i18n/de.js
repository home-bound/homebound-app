export default {
    "home": {
        "title": "Heute 🌱",
        "subtitle": "Jeder von uns kann mit seinem täglichen Verhalten etwas ändern. Verfolge deinen täglichen Impact und beginne jetzt zu handeln.",
        "Your daily impact": "Dein täglicher Impact",
        "You": "Du",
        "Update Profile": "Profil aktualisieren",
        "Review my day": "Review my day",
        "daily-saved": "Du hast bislang {{n}} andere gerettet.",
        "daily-endangered": "Du hast bislang {{n}} andere in Gefahr gebracht",
        "daily-score": "Dein heutiger Score ist {{n}}/100.",
        "Your impact on the curve": "Dein Impact auf die Kurve",
        "education": {
            "Social-Distancing-Title": "Was ist soziale Distanzierung?",
            "Social-Distancing-Desc": "Soziale Distanzierung bedeutet, dass man sich außerhalb der Versammlungsorte aufhält, Massenansammlungen vermeidet und wenn möglich Abstand (ca. 2 Meter) zu anderen hält...",
            "Hygiene-Title": "Wie kann man gute Hygiene gewährleisten?",
            "Hygiene-Desc": "Hier sind ein paar Möglichkeiten, wie Du gute Hygiene praktizieren und die Verbreitung der Infektion vermeiden kannst, falls Du krank bist..."
          }
    },
    "review": {
        "title": "{{name}}, wie war dein Tag?",
        "default name": "Hi",
        "Done": "Erledigt",
        "options": {
            "PeopleMet": {
                "description": "Mit wie vielen Personen hast du täglich persönlichen Kontakt?",
                // "description": "Mit wie vielen Personen hast du täglich persönlichen Kontakt? Personen, bei denen du den vorgegebenen Mindestabstand von 1,50 Meter nicht einhalten kannst/wolltest.",
                "values": {
                    1: "1",
                    2: "2",
                    3: "3",
                    4: "4",
                    5: "5 oder mehr",
                },
                "label": "mit {{n}} Person(en)"
            },
            "Hygiene": {
                "description": "Wie konsequent verfolgst du die Hygiene-Etikette?",
                // "description": "Wie konsequent verfolgst du die Hygiene-Etikette? Regelmäßiges, gründliches Hände waschen Hände aus dem Gesicht fernhalten Niese in den Ellenbogen! Putzen die Nase mit einem Taschentuch und wirf dieses anschließend weg und wasche dir die Hände Im Krankheitsfall Abstand von anderen halten Vermeide den Kontakt mit häufig angefassten Stellen und Objekten in der Öffentlichkeit Wunden schützen.",
                "values": {
                    0: "sehr stark, nach jedem Kontakt",
                    1: "häufig, aber nicht immer",
                    2: "nur bedingt",
                    3: "sehr selten"
                }
            },
        }
    },
    "profile": {
        "title": "Hi {{name}} 👋🏻",
        "Demographics": "Demografie",
        "Social distancing": "Social distancing",
        "Risk Areas": "Risikogebiete",
        "Risk Group": "Risikogruppe",
        "default name": "there",
        "options": {
            "Name": {
                "description": "Wie heißt du?",
            },
            "Age": {
                "description": "Wie alt bist du?"
            },
            "BiologicalSex": {
                "description": "Dein Geschlecht?",
                "values": {
                    "male": "männlich",
                    "female": "weiblich",
                    "diverse": "divers"
                }
            },
            "Height": {
                "description": "Deine Körpergröße?"
            },
            "Weight": {
                "description": "Dein Gewicht?"
            },
            "Restrictions": {
                "description": "Wie findest du es, dass Länder und Städte das soziale Leben (Bars, Kinos etc.) weitestgehend einschränken?",
                "values": {
                    0: "notwendig",
                    1: "nachvollziehbar",
                    2: "ist mir egal",
                    3: "finde ich schlecht",
                    4: "völlig übertrieben",
                }
            },
            "VisitedRiskArea": {
                "description": "In welchen Risikogebieten warst du in den letzten 2 Wochen mindestens einen Tag?",
                "values": {
                    none: 'in keinem der Risikogebiete',
                    germany: 'Deutschland: Kreis Heinsberg, NRW',
                    austria: 'Österreich: Bundesland Tirol',
                    france: 'Frankreich: Elsass, Lothringen & Champagne-Ardenne',
                    italy: 'Italien: gesamtes Land',
                    spain: 'Spanien: Madrid',
                    iran: 'Iran: gesamtes Land',
                    usa: 'USA: Kalifornien, Washington & New York',
                    china: 'China: Provinz Hubei inkl. Stadt Wuhan',
                    south_korea: 'Südkorea: Provinz Gyeongsangbuk-do',
                    multiple: 'mehr als 1 Risikogebiet',
                }
            },
            "VisitedRiskAreaDuration": {
                "description": "Wie lange hast du dich in einem Covid-19-Risikogebiet* aufgehalten?",
                "values": {
                    0: 'garnicht',
                    1: '1 Tag',
                    2: 'Kürzer als 3 Tage',
                    3: '3 bis 7 Tage',
                    7: 'länger als 7 Tage',
                }
            },
            "HomeOfficeApproval": {
                "description": "Wenn du die Möglichkeit hättest freiwillig ins Home-Office zu gehen, würdest du diese wahrnehmen?",
                "values": {
                    0: 'Ja, auf jeden Fall',
                    1: 'Ich bin mir nicht sicher',
                    2: 'Ja, aber es geht nicht',
                    3: 'Nein, auf keinen Fall',
                }
            },
            "BehaviourApproval": {
                "description": "Wie wahrscheinlich ist es, dass du dein Verhalten (Einschränkung sozialer Kontakte etc.) in den nächsten Wochen änderst?",
                "values": {
                    0: 'Ich begebe mich in häusliche Quarantäne',
                    1: 'Ich reduziere meine sozialen Kontakte',
                    2: 'Ich plane meinen sozialen Kontakte einzuschränken',
                    3: 'Ich habe mir darüber noch keine Gedanken gemacht',
                    4: 'Ich ändere momentan nichts an meinem Verhalten',
                }
            },
            "RiskPersonsAround": {
                "description": "Stehen du in direktem Kontakt zu Personen aus einer Risikogruppe* (Personen ab 50 Jahren und/oder mit Vorerkrankungen wie z.B. Herzkreislauferkrankungen, Diabetes, Erkrankungen des Atmungssystems, der Leber und der Niere sowie Krebserkrankungen)?",
                "values": {
                    0: 'nein',
                    1: '1x pro Woche',
                    2: 'Mehrmals pro Woche',
                    3: 'Täglich',
                }
            },
        },
        "Update Profile": "Profil updaten"
    }
}