export default {
  "home": {
    "title": "Today 🌱",
    "subtitle": "Everyone of us can change something with their daily behaviour. See your daily impact and start acting now.",
    "Your daily impact": "Your daily impact",
    "You": "You",
    "Update Profile": "Update Profile",
    "Review my day": "Review my day",
    "daily-saved": "You saved {{n}} people so far",
    "daily-endangered": "You endangered {{n}} people so far",
    "daily-score": "Your score today is {{n}}/100.",
    "Your impact on the curve": "Your impact on the curve",
    "education": {
      "Social-Distancing-Title": "What is social distancing?",
      "Social-Distancing-Desc": "Social distancing means remaining out of congregate settings, avoiding mass gatherings, and maintaining distance (approximately 2 meters) from others when possible...",
      "Hygiene-Title": "How to practice good hygiene?",
      "Hygiene-Desc": "Here are a few ways by which you can practice good hygiene and avoid spreading the infection, in case you are sick..."
    }
  },
  "review": {
    "title": "{{name}}, how was your day?",
    "default name": "Hi",
    "Done": "Done",
    "options": {
      "PeopleMet": {
        "description": "How many people did you have personal contact with today?",
        // "description": "Mit wie vielen Personen hast du täglich persönlichen Kontakt? Personen, bei denen du den vorgegebenen Mindestabstand von 1,50 Meter nicht einhalten kannst/wolltest.",
        "values": {
          1: "1",
          2: "2",
          3: "3",
          4: "4",
          5: "5 oder mehr",
        },
        "label": "with {{n}} people"
      },
      "Hygiene": {
        "description": "How consistently do you follow the hygiene etiquette?",
        // "description": "Wie konsequent verfolgst du die Hygiene-Etikette? Regelmäßiges, gründliches Hände waschen Hände aus dem Gesicht fernhalten Niese in den Ellenbogen! Putzen die Nase mit einem Taschentuch und wirf dieses anschließend weg und wasche dir die Hände Im Krankheitsfall Abstand von anderen halten Vermeide den Kontakt mit häufig angefassten Stellen und Objekten in der Öffentlichkeit Wunden schützen.",
        "values": {
          0: "very intense, after every contact",
          1: "often, but not always",
          2: "only sometimes",
          3: "very rarely"
        }
      },
    }
  },
  "profile": {
    "title": "Hi {{name}} 👋🏻",
    "Demographics": "Demographics",
    "Social distancing": "Social distancing",
    "Risk Areas": "Risk Areas",
    "Risk Group": "Risk Group",
    "default name": "there",
    "options": {
      "Name": {
        "description": "What's your name?",
      },
      "Age": {
        "description": "How old are you?"
      },
      "BiologicalSex": {
        "description": "Your gender?",
        "values": {
          "male": "male",
          "female": "female",
          "diverse": "diverse"
        }
      },
      "Height": {
        "description": "Your height?"
      },
      "Weight": {
        "description": "Your weight?"
      },
      "Restrictions": {
        "description": "How do you find countries and cities to limit social life (bars, cinemas etc.) as much as possible?",
        "values": {
          0: "necessary",
          1: "understandable",
          2: "I don't care",
          3: "I think it's bad",
          4: "completely exaggerated",
        }
      },
      "VisitedRiskArea": {
        "description": "In which risk areas have you been at least one day in the last 2 weeks?",
        "values": {
          none: 'in none of the risk areas',
          germany: 'Germany: District of Heinsberg, NRW',
          austria: 'Austria: State of Tyrol',
          france: 'France: Alsace, Lorraine & Champagne-Ardenne',
          italy: 'Italy: entire country',
          spain: 'Spain: Madrid',
          iran: 'Iran: entire country',
          usa: 'USA: California, Washington & New York',
          china: 'China: Hubei Province incl. Wuhan City',
          south_korea: 'South Korea: Gyeongsangbuk-do Province',
          multiple: 'more than 1 risk area',
        }
      },
      "VisitedRiskAreaDuration": {
        "description": "How long have you been in a Covid 19 risk area* ?",
        "values": {
          0: 'not at all',
          1: 'for 1 day',
          2: '2 days',
          3: '3 to 7 days',
          7: 'more than 7 days',
        }
      },
      "HomeOfficeApproval": {
        "description": "If you had the option to voluntarily go to your home office, would you take advantage of it?",
        "values": {
          0: 'Yes, in any case',
          1: 'I\'m not sure',
          2: 'Yes but it does not work',
          3: 'No, definitely not',
        }
      },
      "BehaviourApproval": {
        "description": "How likely is it that you will change your behavior (restriction of social contacts etc.) in the next few weeks?",
        "values": {
          0: 'I\'m going to quarantine at home',
          1: 'I reduce my social contacts',
          2: 'I plan to limit my social contacts',
          3: 'I haven\'t thought about it yet',
          4: 'I\'m currently not changing anything in my behavior',
        }
      },
      "RiskPersonsAround": {
        "description": "Are you in direct contact with people from a risk group* (people aged 50 and over and / or with previous diseases such as cardiovascular diseases, diabetes, diseases of the respiratory system, liver and kidney and cancer)?",
        "values": {
          0: 'No',
          1: 'Once per week',
          2: 'Several times a week',
          3: 'Daily',
        }
      },
    },
    "Update Profile": "Update Profile"
  }
}
