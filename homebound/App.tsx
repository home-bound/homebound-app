import React, {useState, useEffect} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import * as _ from 'lodash';

import Home from './components/Home/Home';
import Profile from './components/Profile/Profile';
import {UserProfileContext} from './utils/context/userProfileContext';
import {UserProfile} from 'root-types';
import {save, load} from './utils/storage';
import './utils/i18n/i18n';

const Stack = createStackNavigator();

const USER_PROFILE_KEY = 'user-profile';

export type RootStackParamList = {
  Profile: {};
};

const App = () => {
  const [profile, setProfile] = useState<UserProfile>({});

  useEffect(() => {
    const loadData = async () => {
      const persistedProfile = await load(USER_PROFILE_KEY);
      if (persistedProfile) {
        setProfile(persistedProfile);
      }
    };
    loadData();
  }, []);

  const updateProfile = (change: Partial<UserProfile>) => {
    const newProfile: UserProfile = _.merge({}, profile, change);
    setProfile(newProfile);
    save(USER_PROFILE_KEY, newProfile);
    console.log('Updated profile:' , newProfile);    
  };

  return (
    <UserProfileContext.Provider value={{profile, updateProfile}}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Home}
            options={{title: 'Daily Impact'}}
          />
          <Stack.Screen name="Profile" component={Profile} />
        </Stack.Navigator>
      </NavigationContainer>
    </UserProfileContext.Provider>
  );
};

export default App;
